# README #

Welcome to a simple comments list for your open Bitbucket pull requests!

**Please note:** using a Chromium based browser delivers the best experience. There are known hiccups with Safari and this hasn't been tested in IE.

This is an extremely crude use and implementation of the Bitbucket API thrown together overnight, so please don't pass too much judgement. Find out more about their API here: [https://developer.atlassian.com/bitbucket/api/2/reference/](https://developer.atlassian.com/bitbucket/api/2/reference/)


### What do I need to do before running this? ###

* Grab an app specific password from Bitbucket: [https://bitbucket.org/account/settings/app-passwords/](https://bitbucket.org/account/settings/app-passwords/)
* If you want, read up on browser ```localStorage``` vs ```sessionStorage```, this is how your login info is stored (as a base64 string)
    * [https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage)
    * [https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage)


### How do I run this? ###

You can run this any way you want, even just opening the file directly in the browser.

If you have a localhost instance setup via an Apache server, you can use that, too.

You can also use a simple python server, which should put you running at http://0.0.0.0:8000/

If Python version 3.x
```sh
python3 -m http.server
```

If Python version 2.x
```sh
python -m SimpleHTTPServer
```
