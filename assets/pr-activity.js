loginForm.onsubmit = async (e) => {
    e.preventDefault();

    let loginFormDate = new FormData(loginForm);
    let base64UP = btoa(loginFormDate.get('username') + ':' + loginFormDate.get('password'));

    if (loginFormDate.get('keep-logged-in')) {
        window.localStorage.setItem('localLoginUsername', loginFormDate.get('username'));
        window.localStorage.setItem('base64UsernamePassword', base64UP);
        window.localStorage.setItem('localWorkspace', loginFormDate.get('workspace'));
    } else {
        window.sessionStorage.setItem('localLoginUsername', loginFormDate.get('username'));
        window.sessionStorage.setItem('base64UsernamePassword', base64UP);
        window.sessionStorage.setItem('localWorkspace', loginFormDate.get('workspace'));
    }

    if (loginFormDate.get('keep-me-secure')) {
        window.localStorage.setItem('onbeforeunload', true);
    }

    checkUser();
};

updateAutoRefresh = () => {
    if (window.localStorage.getItem('autoRefresh') === 'true') {
        window.autoRefreshInterval = setInterval(() => {
            fetchPullRequests();
        }, 60*1000);
    } else {
        clearInterval(window.autoRefreshInterval);
    }
};

toggleSections = () => {
    let localStorageLoginUsername = window.localStorage.getItem('localLoginUsername');
    let sessionStorageLoginUsername = window.sessionStorage.getItem('localLoginUsername');

    if (localStorageLoginUsername || sessionStorageLoginUsername) {
        document.querySelector('#loginForm').style.display = 'none';
        document.querySelector('#logged-in-content').style.display = 'block';

        let userName = (localStorageLoginUsername) ? localStorageLoginUsername : sessionStorageLoginUsername;
        document.querySelector('.logged-in-username').innerHTML = userName;

        fetchPullRequests();
    } else {
        document.querySelector('#loginForm').style.display = 'block';
        document.querySelector('#logged-in-content').style.display = 'none';
        document.querySelector('.logged-in-username').innerHTML = "";
    }
};

fetchPullRequests = () => {
    fetchData(`https://api.bitbucket.org/2.0/pullrequests/${window.loginUsername}?state=OPEN&pagelen=50`, (data) => {
        renderPullRequests(data.values);
    });
};

renderPullRequests = (openPRs) => {
    document.querySelector('.activity-rows').innerHTML = '';
    let sortedOpenPRs = JSON.parse(JSON.stringify(openPRs));

    sortedOpenPRs.sort(function(a, b) {
        a = new Date(a.updated_on);
        b = new Date(b.updated_on);
        return a > b ? -1 : a < b ? 1 : 0;
    });

    for (const key in sortedOpenPRs) {
        let tableRow = `<tr prid="${sortedOpenPRs[key].id}"></tr>`;
        document.querySelector('.activity-rows').insertAdjacentHTML('beforeend', tableRow);

        let prActivity = fetchData(`https://api.bitbucket.org/2.0/repositories/${window.localWorkspace}/${sortedOpenPRs[key].source.repository.name}/pullrequests/${sortedOpenPRs[key].id}`, (data) => {
            let reviewers = data.reviewers.length;
            let approvals = data.participants.filter(participant => participant.approved).length;
            let selfApproved = data.participants.filter(participant => participant.approved && participant.user.uuid === window.userUUID).length;

            let tableRowContents = `
                <td class="pr-repo">
                    <div class="sticky-element toggle-comments" title="Toggle comments" onclick="toggleComments(${sortedOpenPRs[key].id})">
                        <strong>${sortedOpenPRs[key].source.repository.name}</strong>
                        <em>&nbsp;⤷&nbsp;${sortedOpenPRs[key].destination.branch.name}</em>
                    </div>
                    <div class="sticky-element pr-approval-status">
                        Reviewers: <strong>${reviewers}</strong>
                        <br>
                        Approvals: <strong>${approvals}</strong>
                        <br>
                        Self-Approved: <strong>${ `${selfApproved ? 'Yes' : 'No'}` }</strong>
                    </div>
                </td>
                <td class="pr-comments">
                    <div class="sticky-element pr-title">
                        <a href="${sortedOpenPRs[key].links.html.href}" target="_blank" class="title-link">
                            <strong>${sortedOpenPRs[key].title}</strong> (${sortedOpenPRs[key].comment_count} comments<span class="comments-today-${sortedOpenPRs[key].id}"></span>)
                        </a>
                    </div>
                </td>`;
            document.querySelector(`tr[prid="${sortedOpenPRs[key].id}"]`).insertAdjacentHTML('beforeend', tableRowContents);

            renderPullRequestActivity(sortedOpenPRs[key].source.repository.name, sortedOpenPRs[key].id);
        });
    }
};

renderPullRequestActivity = (prRepo, prID) => {
    fetchData(`https://api.bitbucket.org/2.0/repositories/${window.localWorkspace}/${prRepo}/pullrequests/${prID}/comments?pagelen=100`, (data) => {
        let prComments = '';
        let sortedComments = JSON.parse(JSON.stringify(data.values));
        let commentsToday = 0;

        sortedComments.sort(function(a, b) {
            a = new Date(a.created_on);
            b = new Date(b.created_on);
            return a > b ? -1 : a < b ? 1 : 0;
        });

        for (const key in sortedComments) {
            // Top level comment
            if (sortedComments[key].parent === undefined) {
                if (!sortedComments[key].deleted) {
                    let dt = new Date(sortedComments[key].created_on);
                    let dateString = (isDateToday(dt)) ? 'Today' : dt.toDateString();
                    let cleanDate = `${dateString}, ${dt.toLocaleTimeString()}`;
                    let file = '';

                    if (isDateToday(dt)) { commentsToday++; }

                    if (sortedComments[key].inline !== undefined) {
                        file = `${sortedComments[key].inline.path}:${sortedComments[key].inline.to}`;
                    }

                    prComments += `
                    <div commentid="${sortedComments[key].id}" class="pr-comment">
                        <a href="${sortedComments[key].links.html.href}" target="_blank" class="comment-link">
                            <strong>${sortedComments[key].user.display_name}</strong> (${cleanDate})
                        </a>

                        ${ `${file !== '' ? `<span class="comment-file">${file}</span>` : ''}` }

                        ${sortedComments[key].content.html}
                        <div class="comment-replies"></div>
                    </div>`;
                }
            }
        }
        document.querySelector('tr[prid="' + prID + '"] > td.pr-comments').insertAdjacentHTML('beforeend', prComments);

        for (const key in data.values) {
            let commentReply = '';
            // Reply comments
            if (data.values[key].parent !== undefined) {
                let dt = new Date(data.values[key].created_on);
                let dateString = (isDateToday(dt)) ? 'Today' : dt.toDateString();
                let cleanDate = `${dateString}, ${dt.toLocaleTimeString()}`;

                if (isDateToday(dt)) { commentsToday++; }

                if (!data.values[key].deleted) {
                    commentReply = `
                    <div commentid="${data.values[key].id}" class="pr-comment pr-comment-reply">
                        <a href="${data.values[key].links.html.href}" target="_blank" class="comment-link">
                            <strong>${data.values[key].user.display_name}</strong> (${cleanDate}):
                        </a>
                        ${data.values[key].content.html}
                        <div class="comment-replies"></div>
                    </div>`;
                }

                if (!document.querySelector(`tr[prid="${prID}"] > td.pr-comments div[commentid="${data.values[key].parent.id}"]`)) {
                    document.querySelector('tr[prid="' + prID + '"] > td.pr-comments').insertAdjacentHTML('beforeend', commentReply);
                } else {
                    document.querySelector(`tr[prid="${prID}"] > td.pr-comments div[commentid="${data.values[key].parent.id}"] .comment-replies`).insertAdjacentHTML('beforeend', commentReply);
                }
            }
        }

        if (commentsToday) {
            document.querySelector(`span.comments-today-${prID}`).insertAdjacentHTML('beforeend', `, ${commentsToday} today`);
        }
    });
};

isDateToday = (givenDate) => {
    const today = new Date();
    return givenDate.getDate() == today.getDate() &&
            givenDate.getMonth() == today.getMonth() &&
            givenDate.getFullYear() == today.getFullYear();
};

toggleComments = (prID) => {
    document.querySelector('tr[prid="' + prID + '"]').classList.toggle('hide-comments');
};

checkOnbeforeunload = () => {
    if (window.localStorage.getItem('onbeforeunload')) {
        obliterateStorage();
    }
}

obliterateStorage = () => {
    document.querySelector('.activity-rows').innerHTML = '';
    document.querySelector('.logged-in-username').innerHTML = '';

    clearInterval(window.autoRefreshInterval);

    window.localStorage.removeItem('localLoginUsername');
    window.localStorage.removeItem('base64UsernamePassword');
    window.localStorage.removeItem('localWorkspace');
    window.localStorage.removeItem('onbeforeunload');
    window.localStorage.removeItem('autoRefresh');

    window.sessionStorage.removeItem('localLoginUsername');
    window.sessionStorage.removeItem('base64UsernamePassword');
    window.sessionStorage.removeItem('localWorkspace');

    delete window.loginUsername;
    delete window.base64UsernamePassword;
    delete window.localWorkspace;
    delete window.autoRefreshInterval;
    delete window.userUUID;

    toggleSections();
};

checkUser = () => {
    window.userUUID = ''; // clear uuid
    let localStorageLoginUsername = window.localStorage.getItem('localLoginUsername');
    window.loginUsername = (localStorageLoginUsername) ? localStorageLoginUsername : window.sessionStorage.getItem('localLoginUsername');
    window.base64UsernamePassword = (localStorageLoginUsername) ? window.localStorage.getItem('base64UsernamePassword') : window.sessionStorage.getItem('base64UsernamePassword');
    window.localWorkspace = (localStorageLoginUsername) ? window.localStorage.getItem('localWorkspace') : window.sessionStorage.getItem('localWorkspace');

    generateAlert('error', 'hide');
    document.querySelector('body').classList.add('loading');

    fetchData('https://api.bitbucket.org/2.0/user/', (data) => {
        window.userUUID = data.uuid;
        document.querySelector('body').classList.remove('loading');
        document.querySelector('#loginForm').reset();
        toggleSections();
    });
};

fetchData = (url, callback) => {
    generateAlert('error', 'hide');
    document.querySelector('body').classList.add('loading');

    let newRequest = new Request(url, {
        method: 'GET',
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Basic ${window.base64UsernamePassword}`,
        },
        referrerPolicy: 'no-referrer'
    });

    fetch(newRequest)
        .then(response => response.json())
        .then(data => {
            document.querySelector('body').classList.remove('loading');
            callback(data);
        })
        .catch((error) => {
            console.log(error);
            generateAlert('error', 'show', 'ERROR: ' + error);
            document.querySelector('body').classList.remove('loading');
        });
};

generateAlert = (type, action, msg) => {
    let alertTypeClass = (type === 'error') ? '.error-alert' : '.success-alert';
    let message;

    if (!msg) {
        message = (type === 'error') ? 'Error' : 'Success';
    } else {
        message = msg;
    }

    if (action === 'show') {
        document.querySelector(alertTypeClass).classList.add('show');
        document.querySelector(alertTypeClass).innerText = message;
    } else {
        document.querySelectorAll('[class*="-alert"]').forEach(alert => alert.classList.remove('show'));
    }
};

(() => {
    if (window.localStorage.getItem('localLoginUsername') || window.sessionStorage.getItem('localLoginUsername')) {
        checkUser();
    } else {
        // For hiding PR activity and showing the "login" form
        toggleSections();
    }

    // Setup and figure out if the user set auto refresh, update checkbox visual
    window.autoRefreshInterval;
    const autoRefresh = document.querySelector('#auto-refresh');
    autoRefresh.checked = (window.localStorage.getItem('autoRefresh') === 'true');
    updateAutoRefresh();

    // listen for change of the auto refresh checkbox
    autoRefresh.addEventListener('change', (event) => {
        window.localStorage.setItem('autoRefresh', event.currentTarget.checked);
        updateAutoRefresh();
    });
})();
